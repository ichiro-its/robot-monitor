# ObjectDetection

## Install OpenFrameworks

- Download and install **OpenFrameworks** from `https://openframeworks.cc/download/`.

## Add ofxDatGui

- Clone this repository `https://github.com/braitsch/ofxDatGui` inside `(path_to_OFX)/addons`.

## Install libyaml-cpp
- Install **libyaml-cpp**.
  ``` sh
  ~$ sudo apt install libyaml-cpp-dev
  ```

## Compilation
- To link **libyaml-cpp** add `$(LFLAGS)` to `(path_to_OFX)/libs/openFrameworksCompiled/project/makefileCommon/compile.project.mk`
  at line 405.
- Modify `OF_ROOT` to your own **OpenFrameworks** path inside `Makefile`.
- Compile **Robot-Monitor**.
  ``` sh
  ~$ make
  ```

## Run
- Run **Robot-Monitor**.
  ``` sh
  ~$ make RunRelease
  ```
  or
  ``` sh
  ~$ cd bin
  ~$ ./robot-monitor
  ```