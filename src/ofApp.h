#pragma once

#include <vector>
#include "ofMain.h"
#include "ofxDatGui.h"
#include "Listener.h"
#include "Game.h"


class ofApp : public ofBaseApp{

private:

	ofImage* imageHeader;

	ofxDatGuiButton* buttonExit;
	ofxDatGuiButton* buttonFullscreen;
	ofxDatGuiButton* buttonReset;
	ofxDatGuiButton* buttonFlip;

	Game* game;
	Listener* robotListener;
	Listener* gameListener;

	bool isFullscreen;

public:

	int screenWidth = 1366;
	int screenHeight = 768;

	bool full = true;
	int fullCount = 0;
	float ratioW = 1;
	float ratioH = 1;

	void setup();
	void update();
	void draw();

	void onButtonEvent (ofxDatGuiButtonEvent event);
	void keyPressed (int key);
	void keyReleased (int key);
	void mouseMoved (int x, int y );
	void mouseDragged (int x, int y, int button);
	void mousePressed (int x, int y, int button);
	void mouseReleased (int x, int y, int button);
	void mouseEntered (int x, int y);
	void mouseExited (int x, int y);
	void windowResized (int w, int h);
	void dragEvent (ofDragInfo dragInfo);
	void gotMessage (ofMessage msg);
};
