#pragma once

#include <string>
#include <vector>
#include "ofMain.h"
#include "ofxDatGui.h"
#include "Robot.h"
#include "Game.h"

class RobotGUI {

private:

	int positionX;
	int positionY;

	Robot* robot = NULL;
	Game* game = NULL;

	std::string tempName = "";
	int tempActive = -1;
	int tempPenalise= 0;
	int tempRole = -1;
	int tempSide = -1;
	int tempState = -1;
	int tempSearch = -1;
	int tempPenalSec = 0;
	float tempPan = -1;
	float tempTilt = -1;
	float tempDirection = -1;
	float tempShootDirection = -999;
	float tempPositionX = -999;
	float tempPositionY = -999;
	int tempDetBall = -1;
	float tempBallX = -999;
	float tempBallY = -999;

	ofxDatGuiFolder* folder;
	ofxDatGuiTextInput* inputRole;
	ofxDatGuiTextInput* inputState;
	ofxDatGuiTextInput* inputDirection;
	ofxDatGuiTextInput* inputShootDir;
	ofxDatGuiTextInput* inputPosition;
	ofxDatGuiTextInput* inputBallPos;
	ofxDatGuiTextInput* inputHead;

	void SetTheme (int handler);
	void SetThemePen();

	enum
	{
		INIT = 0,
		PLAYING = 1,
		PENAL = 2
	};
	
	enum
    {
        STATE_FALLEN            = 0,
        STATE_PENALISE          = 1,
        STATE_RELEASE           = 2,
        STATE_KICKOFF           = 3,
        STATE_PLAY              = 4,
        STATE_DEFEND_POSITION   = 5,
        STATE_DEFEND_KEEPER     = 14,
        STATE_PATROL_POSITION   = 6,
        STATE_FOLLOW_BALL       = 7,
        STATE_HOLD_BALL         = 8,
        STATE_LOST_BALL         = 9,
        STATE_DRIBBLE_BALL      = 10,
        STATE_POSITION_BALL     = 11,
        STATE_KICK_BALL         = 12,
        STATE_FINISH            = 13,
    };
public:

	static std::vector<RobotGUI*>* Instances;

	int id;
	int theme_handler;
	
	RobotGUI (int id, int positionX, int positionY);

	void Update ();
	void Draw ();
	void setGuiPosition(int x, int y);
};