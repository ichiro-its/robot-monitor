#include <yaml-cpp/yaml.h>

#include "Listener.h"
#include "stdio.h"

Listener::Listener (int port, bool showLog) {
	printf ("init\n");

	newMessage = false;

	resetLog();

	this->port = port;
	this->showLog = showLog;

	udpConnection.Create ();
	udpConnection.Bind (this->port);
	udpConnection.SetNonBlocking (true);

  YAML::Node config = YAML::LoadFile("../bin/idteam.yaml");
	YAML::Node id_team = config["ID_TEAM"];

	id_kri = id_team["kri"].as<int>();
	id_rc = id_team["robocup"].as<int>();

	printf("kri: %d\nrobocup: %d\n", id_kri, id_rc);
}

Listener::~Listener () {
	fclose (file);
	udpConnection.Close ();
}

void Listener::Update () {
	int n = udpConnection.Receive (message, 1024);
	// newMessage = false;
	if (n > 0) {
		std::string address;
		udpConnection.GetRemoteAddr (address, port );
		newMessage = true;
		if (showLog) {
			// printf ("from: %s port: %d message: %s\n", address.c_str (), port, message);
		}
	}
	else newMessage = false;
}

void Listener::resetLog()
{
	file = fopen ("../bin/Log.txt", "w");
	fprintf (file, "===LOG===\n\n");
	fclose (file);

	file = fopen ("../bin/Log.txt", "a");
}
