#include "stdio.h"
#include "ofMain.h"
#include "ofApp.h"

#include <yaml-cpp/yaml.h>

int main(int argc, char* argv[]){
	printf ("initialize..\n");
	ofSetupOpenGL(1366,768,OF_WINDOW);
	ofRunApp(new ofApp());

	return 0;
}
