#pragma once

#include "ofMain.h"

class Field {

private:

	const int fieldWidth = 900;
	const int fieldHeight = 600;
	bool fieldFlipped = false;

	ofImage* imageField;
	ofImage* imageFieldFlipped;
	ofTrueTypeFont* font;

	int positionX;
	int positionY;

	int offsetX;
	int offsetY;
	int width;
	int height;
	int fieldW;
	int fieldH;
	
	double coordinateX;
	double coordinateY;

public:

	static Field* Instance;

	Field (int positionX, int positionY);

	void Update ();
	void Draw ();

	void DrawHover ();
	void SetFlipped (bool isFlipped);
	bool IsFlipped ();

	double FieldToScreenX (double value);
	double FieldToScreenY (double value);
	double ScreenToFieldX (double value);
	double ScreenToFieldY (double value);

	bool InsideField (int x, int y);

	void setRatioBased(int scW, int scH);
};