#include <stdio.h>
#include "ofApp.h"
#include "Robot.h"
#include "RobotGUI.h"
#include "Field.h"
#include "Themes.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetWindowTitle ("ICHIRO - ITS || ROBOT MONITORING");
	isFullscreen = false;
	ofSetFullscreen (isFullscreen);
	ofBackground (79, 193, 67);
	
	printf("screen: %d %d\n", ofGetWindowHeight(), ofGetWindowWidth());

	ThemeRed::Instance = new ThemeRed ();
	ThemeOrange::Instance = new ThemeOrange ();
	ThemeBlue::Instance = new ThemeBlue ();
	ThemePurple::Instance = new ThemePurple ();
	ThemeBlack::Instance = new ThemeBlack ();
	ThemeDarkGrey::Instance = new ThemeDarkGrey ();
	ThemeYellow::Instance = new ThemeYellow ();

	Field::Instance = new Field((screenWidth - 774) / 2, 121);

	Robot::Instances = new std::vector<Robot*> ();
	Robot::Instances->push_back (new Robot (1, "ARATA"));
	Robot::Instances->push_back (new Robot (2, "TOMO"));
	Robot::Instances->push_back (new Robot (3, "HIRO"));
	Robot::Instances->push_back (new Robot (4, "ITHAARO"));

	int left = 21;
	int right = screenWidth - 256 - 21;
	int top = (96 + (520 / 2)) - 12 - (26 * 9);
	int bottom = (96 + (520 / 2)) + 12;
	RobotGUI::Instances = new std::vector<RobotGUI*> ();
	RobotGUI::Instances->push_back (new RobotGUI (1, left, top));
	RobotGUI::Instances->push_back (new RobotGUI (2, left, bottom));
	RobotGUI::Instances->push_back (new RobotGUI (3, right, top));
	RobotGUI::Instances->push_back (new RobotGUI (4, right, bottom));

	imageHeader = new ofImage ("Header3.png");
	imageHeader->resize(1366,125);
	imageHeader->setAnchorPoint (0, 0);

	right = 1366 - ((1366 - 960) / 2);
	top = 768 - (27 * 4);
	buttonExit = new ofxDatGuiButton ("EXIT");
	buttonExit->setTheme (ThemeDarkGrey::Instance);
	buttonExit->setWidth (225);
	buttonExit->setPosition (250 + (225 * 3), top);
	buttonExit->onButtonEvent (this, &ofApp::onButtonEvent);

	buttonFullscreen = new ofxDatGuiButton ("WINDOW");
	buttonFullscreen->setTheme (ThemeDarkGrey::Instance);
	buttonFullscreen->setWidth (225);
	buttonFullscreen->setPosition (250 + (225 * 2), top);
	buttonFullscreen->onButtonEvent (this, &ofApp::onButtonEvent);

	buttonReset = new ofxDatGuiButton ("RESET");
	buttonReset->setTheme (ThemeDarkGrey::Instance);
	buttonReset->setWidth (225);
	buttonReset->setPosition (250 + 225, top);
	buttonReset->onButtonEvent (this, &ofApp::onButtonEvent);

	buttonFlip = new ofxDatGuiButton ("HOME");
	buttonFlip->setTheme (ThemeDarkGrey::Instance);
	buttonFlip->setWidth (225);
	buttonFlip->setPosition (250, top);
	buttonFlip->onButtonEvent (this, &ofApp::onButtonEvent);

	game = new Game ();
	Game::Instance = game;
	
	robotListener = new Listener (4003, false);
	gameListener = new Listener (3838, true);
}

//--------------------------------------------------------------
void ofApp::update(){

	robotListener->Update ();
	gameListener->Update ();

	// system("clear");
	for (Robot* instance : *(Robot::Instances)) {
		// printf("\n\n[ %s ] %s\n", instance->name.c_str(), (instance->active)? "ACTIVE" : "INACTIVE");
		// printf("[ STATE ] ");
		// switch(instance->state)
		// {
		// 	case 0: printf("JATUH\t"); break;
		// 	case 1: printf("PENALISE\t"); break;
		// 	case 2: printf("RELEASE\t"); break;
		// 	case 3: printf("KICK OFF\t"); break;
		// 	case 4: printf("PLAY\t"); break;
		// 	case 5: printf("DEFEND POS\t"); break;
		// 	case 6: printf("PATROL POS\t"); break;
		// 	case 7: printf("CONTROL BALL\t"); break;
		// 	case 8: printf("HOLD BALL\t"); break;
		// 	case 9: printf("LOST BALL\t"); break;
		// 	case 10: printf("DRIBBLE BALL\t"); break;
		// 	case 11: printf("POSITIONING\t"); break;
		// 	case 12: printf("KICK BALL\t"); break;
		// }
		// printf("[ DIRECTION ] %.2f\n", instance->direction);
		// printf("[ POSITION ] %.2f %.2f\n", instance->positionX, instance->positionY);
		// printf("[ BALL ] %.2f %.2f\n", instance->ballX, instance->ballY);
		instance->Update (robotListener);
	}

	for (RobotGUI* instance : *(RobotGUI::Instances)) {
		instance->Update ();
	}

	Field::Instance->Update ();
	game->Update (gameListener);
	
	buttonExit->update ();
	buttonFullscreen->update ();
	buttonReset->update ();
	buttonFlip->update ();
}

//--------------------------------------------------------------
void ofApp::draw(){
	if(full) 
  {
    fullCount++; 
  }
  // printf("fullCount = %d -> ratio W = %d, ratio H = %d\n\n", fullCount, screenWidth, screenHeight);
	if(fullCount>5)
	{
		screenWidth = ofGetWindowWidth();
		screenHeight = ofGetWindowHeight();
		ratioW = (float)screenWidth/1366;
		ratioH = (float)screenHeight/768;
    // printf("if fullCount = %d -> ratio W = %f, ratio H = %f, W = %d, H = %d\n", fullCount, ratioW, ratioH, screenWidth, screenHeight);
	}

	if(fullCount>5)Field::Instance->setRatioBased(screenWidth, screenHeight);
	Field::Instance->Draw ();


	for (Robot* instance : *(Robot::Instances)) {
		if (instance->active != 1) {
			instance->Draw ();
		}
	}

	for (Robot* instance : *(Robot::Instances)) {
		if (instance->active == 1) {
			instance->Draw ();
		}
	}

	if(fullCount>5)
	{
		imageHeader->load("Header3.png");
		imageHeader->resize(1366*ratioW, 125*ratioH);
	}
	imageHeader->draw (0, 0);


	int left = 21 * ratioW;
	int right = (1366 - 256 - 21)*ratioW;
	int top = ((96 + (520 / 2)) - 12 - (26 * 9)) * ratioH;
	int bottom = ((96 + (520 / 2)) + 12) * ratioH;

	for (RobotGUI* instance : *(RobotGUI::Instances)) {
		if(fullCount>5)
		{
			switch(instance->id)
			{
				case 1: instance->setGuiPosition(left, top); break;
				case 2: instance->setGuiPosition(left, bottom); break;
				case 3: instance->setGuiPosition(right, top); break;
				case 4: instance->setGuiPosition(right, bottom); break;
			}
		}
		instance->Draw ();
	}

	game->Draw (ratioW, ratioH);

	right = (1366 - ((1366 - 960) / 2)) * ratioW;
	top = (768 - (27 * 4))*ratioH;

	if(fullCount>5)
	{
		int buttonW = 225 * ratioW;
		buttonExit->setWidth(buttonW);
		buttonExit->setPosition((250 * ratioW) + (buttonW * 3), top);
		buttonFullscreen->setWidth(buttonW);
		buttonFullscreen->setPosition((250 * ratioW) + (buttonW * 2), top);
		buttonReset->setWidth(buttonW);
		buttonReset->setPosition((250 * ratioW) + (buttonW), top);
		buttonFlip->setWidth(buttonW);
		buttonFlip->setPosition((250 * ratioW), top);
	}

	buttonExit->draw ();
	buttonFullscreen->draw ();
	buttonReset->draw ();
	buttonFlip->draw ();

	Field::Instance->DrawHover ();
	if(fullCount>5)
	{
		fullCount = 0;
		full = false;
	}
}

//--------------------------------------------------------------
void ofApp::onButtonEvent (ofxDatGuiButtonEvent event) {
	if (event.target->getLabel () == "EXIT") {
		OF_EXIT_APP (0);
	} else if (event.target->getLabel () == "FULLSCREEN") {
		buttonFullscreen->setLabel ("WINDOW");
    full = true;
		isFullscreen = false;
		ofSetFullscreen (false);
	} else if (event.target->getLabel () == "WINDOW") {
		buttonFullscreen->setLabel ("FULLSCREEN");
    full = true;
		isFullscreen = true;
		ofSetFullscreen (true);
	} else if (event.target->getLabel () == "RESET") {
		for (Robot* instance : *(Robot::Instances)) {
			instance->Reset ();
		}
	} else if (event.target->getLabel () == "HOME") {
		buttonFlip->setLabel ("AWAY");
		Field::Instance->SetFlipped (true);
	} else if (event.target->getLabel () == "AWAY") {
		buttonFlip->setLabel ("HOME");
		Field::Instance->SetFlipped (false);
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	switch (key) {
		case 'r':
		case 'R':
		{
			for (Robot* instance : *(Robot::Instances)) {
				instance->Reset ();
			}

			// game->Reset(gameListener);
		} break;

		case 'f':
		case 'F':
		{
			isFullscreen = !isFullscreen;
      if (isFullscreen == false) 
      {
        buttonFullscreen->setLabel ("WINDOW"); 
      }
      if (isFullscreen == true) 
      {
        buttonFullscreen->setLabel ("FULLSCREEN"); 
      }
			ofSetFullscreen (isFullscreen);
			printf("screen: %d %d\n", ofGetWindowHeight(), ofGetWindowWidth());
			full = true;
		} break;

		case 'w':
		case 'W':
		{
			Field::Instance->SetFlipped (!Field::Instance->IsFlipped ());
		} break;

		case 27:
		{
			OF_EXIT_APP (0);
		} break;

	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
	ofRectangle rect = ofGetNativeViewport ();
	x = rect.x + ((x - rect.x) * (1366 / (float)rect.width));
	y = rect.y + ((y - rect.y) * (768 / (float)rect.height));
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
	ofRectangle rect = ofGetNativeViewport ();
	x = rect.x + ((x - rect.x) * (1366 / (float)rect.width));
	y = rect.y + ((y - rect.y) * (768 / (float)rect.height));
}

//--------------------------------------------------------------
void ofApp::mousePressed (int x, int y, int button) {
	ofRectangle rect = ofGetNativeViewport ();
	x = rect.x + ((x - rect.x) * (1366 / (float)rect.width));
	y = rect.y + ((y - rect.y) * (768 / (float)rect.height));
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
	ofRectangle rect = ofGetNativeViewport ();
	x = rect.x + ((x - rect.x) * (1366 / (float)rect.width));
	y = rect.y + ((y - rect.y) * (768 / (float)rect.height));
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){
	ofRectangle rect = ofGetNativeViewport ();
	x = rect.x + ((x - rect.x) * (1366 / (float)rect.width));
	y = rect.y + ((y - rect.y) * (768 / (float)rect.height));
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){
	ofRectangle rect = ofGetNativeViewport ();
	x = rect.x + ((x - rect.x) * (1366 / (float)rect.width));
	y = rect.y + ((y - rect.y) * (768 / (float)rect.height));
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}