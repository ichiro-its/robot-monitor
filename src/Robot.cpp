#include "stdio.h"
#include "stdlib.h"
#include "Robot.h"
#include "Field.h"
#include "RobotGUI.h"

#include <iostream>

std::vector<Robot*>* Robot::Instances;

Robot::Robot (int id, std::string name) {

	this->id = id;
	this->name = name;

	Reset ();

	line = new ofPolyline ();

	switch (id) {
		case 1: {
			imageNumber = new ofImage ("Number1.png");
			imageAttacker = new ofImage ("AttackerRed.png");
			imageDefender = new ofImage ("DefenderRed.png");
			imageMiddler = new ofImage ("MiddlerRed.png");
			imageBall = new ofImage ("BallRed.png");
			imageBallDisabled = new ofImage ("BallRedDisabled.png");
		} break;

		case 2: {
			imageNumber = new ofImage ("Number2.png");
			imageAttacker = new ofImage ("AttackerOrange.png");
			imageDefender = new ofImage ("DefenderOrange.png");
			imageMiddler = new ofImage ("MiddlerOrange.png");
			imageBall = new ofImage ("BallOrange.png");
			imageBallDisabled = new ofImage ("BallOrangeDisabled.png");
		} break;

		case 3: {
			imageNumber = new ofImage ("Number3.png");
			imageAttacker = new ofImage ("AttackerBlue.png");
			imageDefender = new ofImage ("DefenderBlue.png");
			imageMiddler = new ofImage ("MiddlerBlue.png");
			imageBall = new ofImage ("BallBlue.png");
			imageBallDisabled = new ofImage ("BallBlueDisabled.png");
		} break;

		case 4: {
			imageNumber = new ofImage ("Number4.png");
			imageAttacker = new ofImage ("AttackerPurple.png");
			imageDefender = new ofImage ("DefenderPurple.png");
			imageMiddler = new ofImage ("MiddlerPurple.png");
			imageBall = new ofImage ("BallPurple.png");
			imageBallDisabled = new ofImage ("BallPurpleDisabled.png");
		} break;
	}

	imageAttackerDisabled = new ofImage ("AttackerDisabled.png");
	imageDefenderDisabled = new ofImage ("DefenderDisabled.png");
	imageMiddlerDisabled = new ofImage ("MiddlerDisabled.png");
	imageShortShoot = new ofImage ("ShortShoot.png");
	imageLongShoot = new ofImage ("LongShoot.png");

	imageNumber->setAnchorPercent (0.5, 0.5);
	imageAttacker->setAnchorPercent (0.5, 0.5);
	imageDefender->setAnchorPercent (0.5, 0.5);
	imageMiddler->setAnchorPercent (0.5, 0.5);
	imageBall->setAnchorPercent (0.5, 0.5);
	imageBallDisabled->setAnchorPercent (0.5, 0.5);
	imageAttackerDisabled->setAnchorPercent (0.5, 0.5);
	imageDefenderDisabled->setAnchorPercent (0.5, 0.5);
	imageMiddlerDisabled->setAnchorPercent (0.5, 0.5);
	imageShortShoot->setAnchorPercent (0, 0.5);
	imageLongShoot->setAnchorPercent (0, 0.5);
}

void Robot::Draw () {

	Field* field = Field::Instance;
	int bx, by, rx, ry;

	rx = Field::Instance->FieldToScreenX (positionX);
	ry = Field::Instance->FieldToScreenY (positionY);

	if (active && Field::Instance->InsideField(ballX, ballY)) {

		bx = Field::Instance->FieldToScreenX (ballX);
		by = Field::Instance->FieldToScreenY (ballY);

		line->clear ();
		line->addVertex (rx, ry);
		line->addVertex (bx, by);
		line->draw ();

		int t = 999;
		int i = -1;
		for (Robot* instance : *(Robot::Instances)) {
			if (instance->detectBall && instance->tilt < t) {
				t = instance->tilt;
				i = instance->id;
			}
		}

		if (i == id && state >= 11) {

			ofPushMatrix ();
			ofTranslate (bx, by, 0);
			ofRotate (shootDirection + (Field::Instance->IsFlipped () ? 180 : 0));

			if (shootLength < 200) imageShortShoot->draw (0, 0);
			else imageLongShoot->draw (0, 0);

			ofPopMatrix ();
		}

		ofPushMatrix ();
		ofTranslate (bx, by, 0);
		if (i == id && state!=8) {
			imageBall->draw (0, 0);
		} else {
			imageBallDisabled->draw (0, 0);
		}

		ofPopMatrix ();
	}

	ofPushMatrix ();
	ofTranslate (rx, ry, 0);
	ofRotate (direction + (Field::Instance->IsFlipped () ? 180 : 0));
	
	if (active == 0) {
		switch (role) {
			case 0: imageAttackerDisabled->draw (0, 0); break;
			case 1: imageDefenderDisabled->draw (0, 0); break;
			case 2: imageMiddlerDisabled->draw (0, 0); break;
			case 3: imageMiddlerDisabled->draw (0, 0); break;

		}
	} else {
		switch (role) {
			case 0: imageAttacker->draw (0, 0); break;
			case 1: imageDefender->draw (0, 0); break;
			case 2: imageMiddler->draw (0, 0); break;
			case 3: imageMiddler->draw (0, 0); break;

		}
	}

	ofPopMatrix ();

	imageNumber->draw (rx, ry);
}

void Robot::Update (Listener* listener) {
	
	if (!listener->newMessage) {
		return;
	}

	// MixedTeamMate MixedTeamParser::parseIncoming(const void* messageData, uint32_t messageLength, int teamID) {
	// test endianness, the default code only supports little endian, so a conversion between host byte order
	// and network byte order should cause a mismatch to the original value
	// assert(htonl(0x12345678) != 0x12345678);

	const MixedTeamCommMessage *message = (const MixedTeamCommMessage*)listener->message;
	if ( MITECOM_MAGIC != message->messageMagic) {
		fprintf(stderr, "Magic value mismatch in received message.\n");
	}

	// we currently support version 1 only
	if (1 != message->messageVersion) {
		fprintf(stderr, "Unsupported protocol received.\n");
	}

	// only handle messages from members of our own team
	if (message->teamID != listener->id_kri && message->teamID != listener->id_rc) {
		// fprintf(stderr, "Musuh cok!\n");
		return;
	}
	
	if(message->teamID == listener->id_rc)
	{
		if(this->id == 1) name = "ITHAARO";
		if(this->id == 2) name = "UMARU";
	}
	else if(message->teamID == listener->id_kri)
	{
		if(this->id == 1) name = "ARATA";
		if(this->id == 2) name = "TOMO";
	}

	std::string name = "", name_l = "", name_h = "";
	if (this->id != message->robotID) {
		return;
	}
	for (uint16_t index = 0;
	     index < message->messageLength;
	     index++)
	{
		MITECOM_KEYTYPE  key   = message->values[index].key;
		MITECOM_DATATYPE value = message->values[index].value;
		if(key==KEY_ICHIRO_ROBOT_STATE) this->state = value;
		else if(key==KEY_ICHIRO_ROBOT_ACTIVE) this->active = value;
		else if(key==KEY_ICHIRO_ROBOT_PENALISE) this->penalise = value;
		else if(key==KEY_ICHIRO_ROBOT_ROLE) this->role = value;
		else if(key==KEY_ICHIRO_ROBOT_SIDE) this->side = value;
		else if(key==KEY_ICHIRO_ROBOT_PAN)
			memcpy(&(this->pan), &value, sizeof(float));
		else if(key==KEY_ICHIRO_ROBOT_TILT)
			memcpy(&(this->tilt), &value, sizeof(float));
		else if(key==KEY_ICHIRO_ROBOT_ORIENTATION)
			memcpy(&(this->direction), &value, sizeof(float));
		else if(key==KEY_ICHIRO_ROBOT_KICK_DIRECTION)
			memcpy(&(this->shootDirection), &value, sizeof(float));
		else if(key==KEY_ICHIRO_ROBOT_KICK_DISTANCE)
			memcpy(&(this->shootLength), &value, sizeof(float));
		else if(key==KEY_ICHIRO_ROBOT_POSITION_X)
			memcpy(&(this->positionX), &value, sizeof(float));
		else if(key==KEY_ICHIRO_ROBOT_POSITION_Y)
			memcpy(&(this->positionY), &value, sizeof(float));
		else if(key==KEY_ICHIRO_ROBOT_DETECT_BALL) this->detectBall = value;
		else if(key==KEY_ICHIRO_ROBOT_BALL_X)
			memcpy(&(this->ballX), &value, sizeof(float));
		else if(key==KEY_ICHIRO_ROBOT_BALL_Y)
			memcpy(&(this->ballY), &value, sizeof(float));
		else if(key==KEY_ICHIRO_ROBOT_NAME_L) name_l = int2String(value);
		else if(key==KEY_ICHIRO_ROBOT_NAME_H) name_h = int2String(value);
	}
	name = name_l + name_h;
	// printf("name: %s id: %d\n", name.c_str(), message->robotID);
	// std::cout << name << std::endl;
	// if(this->name != name)
	// {
	// 	this->name = name;
	// }

	return;
}
	
void Robot::Reset () {
	active = 0;
	penalise = 0;

	role = 0;
	side = 0;
	state = 0;
	search = 0;

	pan = 0;
	tilt = 0;

	direction = 0;
	shootDirection = 0;
	shootLength = 0;

	positionX = -1;
	positionY = -1;

	detectBall = 0;
	ballX = -1;
	ballY = -1;

	kickCount = 0;
	
}