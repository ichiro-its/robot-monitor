#include "Field.h"

Field* Field::Instance;

Field::Field (int positionX, int positionY) {

	this->positionX = positionX;
	this->positionY = positionY;

	offsetX = 44;
	offsetY = 53;
	width = 690;
	height = 460;
	fieldW = 775;
	fieldH = 552;

	imageField = new ofImage ("Field.png");
	imageFieldFlipped = new ofImage ("FieldFlipped.png");

	imageField->resize(fieldW, fieldH);
	imageFieldFlipped->resize(fieldW, fieldH);

	font = new ofTrueTypeFont ();
	font->load ("ofxbraitsch\\fonts\\Verdana.ttf", 12);

	coordinateX = 0;
	coordinateY = 0;
}

void Field::Update () {
	coordinateX = ScreenToFieldX(ofGetMouseX ());
	coordinateY = ScreenToFieldY(ofGetMouseY());

	if (coordinateX < 0) coordinateX = 0;
	else if (coordinateX > fieldWidth) coordinateX = fieldWidth;

	if (coordinateY < 0) coordinateY = 0;
	else if (coordinateY > fieldHeight) coordinateY = fieldHeight;
}

void Field::Draw () {
	if (fieldFlipped) {
		imageFieldFlipped->draw (positionX, positionY);
	} else {
		imageField->draw (positionX, positionY);
	}
}

void Field::DrawHover () {
	int x = FieldToScreenX (coordinateX);
	int y = FieldToScreenY (coordinateY);

	char buffer[32];
	sprintf (buffer, "%.0f, %.0f", coordinateX, coordinateY);

	font->drawString (buffer, x, y);
}

void Field::SetFlipped (bool isFlipped) {
	fieldFlipped = isFlipped;
}

bool Field::IsFlipped () {
	return fieldFlipped;
}

double Field::FieldToScreenX (double value) {
	if (fieldFlipped) {
		return positionX + offsetX + ((fieldWidth - value) * ((double)width / (double)fieldWidth));
	} else {
		return positionX + offsetX + (value * ((double)width / (double)fieldWidth));
	}
}

double Field::FieldToScreenY (double value) {
	if (fieldFlipped) {
		return positionY + offsetY + ((fieldHeight - value) * ((double)height / (double)fieldHeight));
	} else {
		return positionY + offsetY + (value * ((double)height / (double)fieldHeight));
	}
}

double Field::ScreenToFieldX (double value) {
	if (fieldFlipped) {
		return fieldWidth - ((value - (positionX + offsetX)) * ((double)fieldWidth / (double)width));
	} else {
		return (value - (positionX + offsetX)) * ((double)fieldWidth / (double)width);
	}
}

double Field::ScreenToFieldY (double value) {
	if (fieldFlipped) {
		return fieldHeight - ((value - (positionY + offsetY)) * ((double)fieldHeight / (double)height));
	} else {
		return (value - (positionY + offsetY)) * ((double)fieldHeight / (double)height);
	}
}

bool Field::InsideField (int x, int y) {
	return (x > 0) && (x < fieldWidth) && (y > 0) && (y < fieldHeight);
}

void Field::setRatioBased(int scW, int scH)
{
	float ratioW = (float)scW/1366;
	float ratioH = (float)scH/768;

	this->positionX = ((1366 - 774) / 2)*ratioW;
	this->positionY = 121*ratioH;

	this->offsetX = 44 * ratioW;
	this->offsetY = 55 * ratioH;

	this->width = 690 * ratioW;
	this->height = 444 * ratioH;

	this->fieldW = 775 * ratioW;
	this->fieldH = 552 * ratioH;

	imageField->load("Field.png");
	imageField->resize(fieldW, fieldH);

	imageFieldFlipped->load("FieldFlipped.png");
	imageFieldFlipped->resize(fieldW, fieldH);
}