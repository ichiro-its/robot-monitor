#pragma once

#include "ofMain.h"
#include "ofxDatGui.h"
#include "ofApp.h"

class ThemeRed : public ofxDatGuiTheme {
public:
	static ThemeRed* Instance;
	ThemeRed ();
};

class ThemeOrange : public ofxDatGuiTheme {
public:
	static ThemeOrange* Instance;
	ThemeOrange ();
};

class ThemeBlue : public ofxDatGuiTheme {
public:
	static ThemeBlue* Instance;
	ThemeBlue ();
};

class ThemePurple : public ofxDatGuiTheme {
public:
	static ThemePurple* Instance;
	ThemePurple ();
};

class ThemeBlack : public ofxDatGuiTheme {
public:
	static ThemeBlack* Instance;
	ThemeBlack ();
};

class ThemeDarkGrey : public ofxDatGuiTheme {
public:
	static ThemeDarkGrey* Instance;
	ThemeDarkGrey ();
};

class ThemeYellow : public ofxDatGuiTheme {
public:
	static ThemeYellow* Instance;
	ThemeYellow ();
};
