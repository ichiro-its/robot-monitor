#pragma once

#include <string>
#include <vector>
#include "ofMain.h"
#include "ofxDatGui.h"
#include "Listener.h"
#include "mitecom-data.h"

class Robot {

private:

	ofPolyline* line;
	ofImage* imageNumber;
	ofImage* imageAttacker;
	ofImage* imageDefender;
	ofImage* imageMiddler;
	ofImage* imageAttackerDisabled;
	ofImage* imageDefenderDisabled;
	ofImage* imageMiddlerDisabled;
	ofImage* imageBall;
	ofImage* imageBallDisabled;
	ofImage* imageShortShoot;
	ofImage* imageLongShoot;

public:

	static std::vector<Robot*>* Instances;


	int id;
	std::string name;

	int active;
	int penalise;

	uint8_t role;
	uint8_t side;
	uint8_t state;
	uint8_t search;

	float pan;
	float tilt;

	float direction;
	float shootDirection;
	int shootLength;
	
	float positionX;
	float positionY;

	int detectBall;
	float ballX;
	float ballY;

	int kickCount;

	Robot (int id, std::string name);

	void Update (Listener* listener);
	void Draw ();
	void Reset ();
	std::string int2String(uint32_t val)
	{
		std::string str = "";
		for (unsigned int i = 0; i < 4; i++)
		{
			str += (char)((val >> (i * 8)) & 0xFF);
		}

		return str;
	}
};