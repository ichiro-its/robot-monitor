#pragma once

#include <string>
#include "ofMain.h"
#include "ofxDatGui.h"
#include "Listener.h"

#define IDTEAM_KRI 7
#define IDTEAM_RC 19
class Game {

private:

	ofxDatGuiLabel * label;
	ofxDatGuiTextInput* gameState;
	ofTrueTypeFont* text;
	ofTrueTypeFont* text2;
	ofTrueTypeFont* text3;

public:
	struct RobotData
    {
        uint8_t penalty;
        uint8_t secs_till_unpenalised;
        uint8_t yellow_card_count;
        uint8_t red_card_count;
    };

	struct TeamData
    {
        uint8_t team_number;
        uint8_t team_colour;
        uint8_t score;
        uint8_t penalty_shot;
        uint16_t single_shots;
        uint8_t coach_sequence;
        uint8_t coach_message[253]; //253
        RobotData coach;
        RobotData players[11];
    };

	struct MessageData
	{
		uint32_t header;
		uint16_t version;
		// char header[4];
		// uint8_t version;
		uint8_t packet_number;
		uint8_t players_per_team;
		uint8_t game_type;
		uint8_t state;
		uint8_t first_half;
		uint8_t kickoff_team;
		uint8_t secondary_state;
		char secondary_state_info[4];
		uint8_t drop_in_team;
		uint16_t drop_in_time;
		uint16_t secs_remaining;
		uint16_t secondary_time;
		TeamData teams[2];
	};
	
	enum
	{
		INITIAL,
		READY,
		SET,
		PLAY,
		FINISHED
	};

	enum
	{
		NORMAL = 0,
		PENALTYSHOOT = 1,
		OVERTIME = 2,
		TIMEOUT = 3,
		DIRECT_FREE_KICK = 4,
		INDIRECT_FREE_KICK = 5,
		PENALTYKICK = 6,
		CORNER = 7,
		GOAL_KICK = 8,
		THROW_IN = 9,
		DROPBALL = 128,
		UNKNOWN = 255
	};
	
	static Game* Instance;

	std::string lastMessage;
	std::string logMessage;

	int state = 0;
	int secState = 0;
	int secStatePrev = 0;

	int secStateInfo = 0;
	int secStateTime = 700;
	int half = 1;
	int scoreLeft = 0;
	int scoreRight = 0;
	int minute = 0;
	int sec = 0;

	int teamIndex;
	int enemyteamI;
	int penal[11];
	int penalSec[11];

	std::string states = "INITIAL";
	// std::string scoresL = "0 - 0";
	std::string times = "10:00";
	std::string secstates = "NORMAL";
	std::string halfs = "1ST HALF";

	Game ();
	void Update (Listener* listener);
	void Draw (float ratioW, float ratioH);
	void Reset(Listener* listener);
};