#include "Themes.h"
#define FONT_SIZE 10
#define LAYOUT_WIDTH 256;
#define LAYOUT_LABEL_WIDTH 128;

ThemeRed* ThemeRed::Instance;
ThemeRed::ThemeRed () {
	font.size = FONT_SIZE;
	layout.width = LAYOUT_WIDTH;
	layout.labelWidth = LAYOUT_LABEL_WIDTH;

	color.background = ofColor::fromHex (0x751717);
	color.inputAreaBackground = ofColor::fromHex (0xa72121);

	color.guiBackground = ofColor::fromHex(0x000000);
	color.textInput.text = ofColor::fromHex(0xffffff);

	init ();
}

ThemeOrange* ThemeOrange::Instance;
ThemeOrange::ThemeOrange () {
	font.size = FONT_SIZE;
	layout.width = LAYOUT_WIDTH;
	layout.labelWidth = LAYOUT_LABEL_WIDTH;

	color.background = ofColor::fromHex (0x754517);
	color.inputAreaBackground = ofColor::fromHex (0xa76321);

	color.guiBackground = ofColor::fromHex (0x000000);
	color.textInput.text = ofColor::fromHex (0xffffff);

	init ();
}

ThemeBlue* ThemeBlue::Instance;
ThemeBlue::ThemeBlue () {
	font.size = FONT_SIZE;
	layout.width = LAYOUT_WIDTH;
	layout.labelWidth = LAYOUT_LABEL_WIDTH;

	color.background = ofColor::fromHex (0x175975);
	color.inputAreaBackground = ofColor::fromHex (0x2180a7);

	color.guiBackground = ofColor::fromHex (0x000000);
	color.textInput.text = ofColor::fromHex (0xffffff);

	init ();
}

ThemePurple* ThemePurple::Instance;
ThemePurple::ThemePurple () {
	font.size = FONT_SIZE;
	layout.width = LAYOUT_WIDTH;
	layout.labelWidth = LAYOUT_LABEL_WIDTH;

	color.background = ofColor::fromHex (0x6f1775);
	color.inputAreaBackground = ofColor::fromHex (0x9f21a7);

	color.guiBackground = ofColor::fromHex (0x000000);
	color.textInput.text = ofColor::fromHex (0xffffff);

	init ();
}

ThemeBlack* ThemeBlack::Instance;
ThemeBlack::ThemeBlack () {
	font.size = FONT_SIZE;
	layout.width = LAYOUT_WIDTH;

	color.background = ofColor::fromHex (0x000000);
	color.inputAreaBackground = ofColor::fromHex (0x202020);

	color.guiBackground = ofColor::fromHex (0x000000);
	color.textInput.text = ofColor::fromHex (0xffffff);

	init ();
}

ThemeDarkGrey* ThemeDarkGrey::Instance;
ThemeDarkGrey::ThemeDarkGrey () {
	font.size = FONT_SIZE;
	layout.width = LAYOUT_WIDTH;

	color.background = ofColor::fromHex (0x202020);
	color.inputAreaBackground = ofColor::fromHex (0x404040);

	color.guiBackground = ofColor::fromHex (0x000000);
	color.textInput.text = ofColor::fromHex (0xffffff);

	init ();
}

ThemeYellow* ThemeYellow::Instance;
ThemeYellow::ThemeYellow () {
	font.size = FONT_SIZE;
	layout.width = LAYOUT_WIDTH;

	color.background = ofColor::fromHex (0xffff00);
	color.inputAreaBackground = ofColor::fromHex (0xDFC215);

	color.guiBackground = ofColor::fromHex (0x000000);
	color.textInput.text = ofColor::fromHex (0x000000);
	color.label = ofColor::fromHex(0x000000);

	init ();
}