#include "RobotGUI.h"
#include "Robot.h"
#include "Themes.h"

std::vector<RobotGUI*>* RobotGUI::Instances;

RobotGUI::RobotGUI (int id, int positionX, int positionY) {

	this->id = id;

	this->positionX = positionX;
	this->positionY = positionY;

	folder = new ofxDatGuiFolder (" ", ofColor::white);
	folder->setTheme (ThemeBlack::Instance);
	folder->setPosition (positionX, positionY);

	inputRole = folder->addTextInput ("ROLE", " ");
	inputState = folder->addTextInput ("STATE", " ");
	inputDirection = folder->addTextInput ("DIRECTION", " ");
	inputShootDir = folder->addTextInput ("SHOOT DIR", " ");
	inputPosition = folder->addTextInput ("POSITION", " ");
	inputBallPos = folder->addTextInput ("BALL POS", " ");
	inputHead = folder->addTextInput ("HEAD", " ");
	folder->expand ();
	SetTheme (theme_handler);

	theme_handler = 0;
	Update ();
}

void RobotGUI::Update () {
	if (robot == NULL) {
		for (Robot* instance : *(Robot::Instances)) {
			if (instance->id == id) {
				robot = instance;
				break;
			}
		}
	}

	if (robot == NULL) {
		folder->update ();
		return;
	}

	if(game == NULL)
	{
		game = Game::Instance;
	}

	char buffer[32];
	char tempBuffer[32];

	// if (tempName != robot->name || tempActive != robot->active || tempPenalise != robot->penalise) {
	if(!(game!= NULL && game->penal[(this->id)-1]))
	{	
		tempName = robot->name;
		tempActive = robot->active;
		tempPenalise = robot->penalise;
		tempDetBall = robot->detectBall;

		if (tempActive == 1) {
			sprintf (buffer, "#%d %s", id, tempName.c_str ());
		}
		else sprintf (buffer, "#%d %s (INACTIVE)", id, tempName.c_str ());

		folder->setLabel (buffer);
		SetTheme (0);
	}

	if(tempPenalise == 1 || (game!= NULL && game->penal[(this->id)-1])) 
	{
		if(tempPenalSec!=game->penalSec[(this->id)-1])
		{
			sprintf (tempBuffer, "#%d %s (PENALISED) 00:%.2d", this->id, tempName.c_str (), game->penalSec[(this->id)-1]);
			folder->setLabel (tempBuffer);
			SetThemePen();
			SetTheme(theme_handler);
			tempPenalSec = game->penalSec[(this->id)-1];
			if(tempPenalSec == 0) tempPenalise = 1;
		}
	}

	if (tempRole != robot->role) {
		tempRole = robot->role;

		switch (tempRole) {
			case 0: sprintf (buffer, "0 - PYRG"); break;
			case 1: sprintf (buffer, "1 - BTHN"); break;
			case 2: sprintf (buffer, "2 - LEFT WING"); break;
			case 3: sprintf (buffer, "3 - RIGHT WING"); break;

			default: sprintf (buffer, "?? - UNKNOWN"); break;
		}
		inputRole->setText (buffer);
	}

	if (tempState != robot->state) {
		tempState = robot->state;

		switch (tempState) {
			case STATE_FALLEN: sprintf (buffer, "%d - JATUH", tempState); break;
			case STATE_PENALISE: sprintf (buffer, "%d - PENALISE", tempState); break;
			case STATE_RELEASE: sprintf (buffer, "%d - RELEASE", tempState); break;
			case STATE_KICKOFF: sprintf (buffer, "%d - KICKOFF", tempState); break;
			case STATE_PLAY: sprintf (buffer, "%d - PLAY", tempState); break;
			case STATE_DEFEND_POSITION: sprintf (buffer, "%d - DEFPOS", tempState); break;
			case STATE_PATROL_POSITION: sprintf (buffer, "%d - PATROLPOS", tempState); break;
			case STATE_FOLLOW_BALL: sprintf (buffer, "%d - FOLLOWBL", tempState); break;
			case STATE_HOLD_BALL: sprintf (buffer, "%d - HOLD", tempState); break;
			case STATE_LOST_BALL: sprintf (buffer, "%d - LOST", tempState); break;
			case STATE_DRIBBLE_BALL: sprintf (buffer, "%d - DRIBBLE", tempState); break;
			case STATE_POSITION_BALL: sprintf (buffer, "%d - POSBL", tempState); break;
			case STATE_KICK_BALL: sprintf (buffer, "%d - KICK", tempState); break;
			case STATE_DEFEND_KEEPER: sprintf (buffer, "%d - DEF KEEPER", tempState); break;
			case STATE_FINISH: sprintf (buffer, "%d - FINISH", tempState); break;

			default: sprintf (buffer, "%d - UNKNOWN", tempState); break;
		}
		inputState->setText (buffer);
	}

	if (tempDirection != robot->direction) {
		tempDirection = robot->direction;

		sprintf (buffer, "%d", (int)tempDirection);
		inputDirection->setText (buffer);
	}

	if (tempShootDirection != robot->shootDirection) {
		tempShootDirection = robot->shootDirection;

		sprintf (buffer, "%d", (int)tempShootDirection);
		inputShootDir->setText (buffer);
	}

	if (tempPositionX != robot->positionX || tempPositionY != robot->positionY) {
		tempPositionX = robot->positionX;
		tempPositionY = robot->positionY;

		sprintf (buffer, "%d, %d", (int)tempPositionX, (int)tempPositionY);
		inputPosition->setText (buffer);
	}

	if (tempBallX != robot->ballX || tempBallY != robot->ballY) {
		tempBallX = robot->ballX;
		tempBallY = robot->ballY;

		sprintf (buffer, "%d, %d", (int)tempBallX, (int)tempBallY);
		inputBallPos->setText (buffer);
	}

	if (tempPan != robot->pan || tempTilt != robot->tilt) {
		tempPan = robot->pan;
		tempTilt = robot->tilt;

		sprintf (buffer, "%d, %d", (int)tempPan, (int)tempTilt);
		inputHead->setText (buffer);
	}

	folder->update ();
}

void RobotGUI::Draw () {
	folder->draw ();
}

void RobotGUI::SetTheme (int handler) {
	ofxDatGuiTheme* theme = ThemeDarkGrey::Instance;

	if (tempActive == 1) {
		switch (this->id) {
			case 1: theme = ThemeRed::Instance; break;
			case 2: theme = ThemeOrange::Instance; break;
			case 3: theme = ThemeBlue::Instance; break;
			case 4: theme = ThemePurple::Instance; break;
		}
	}

	if(handler) folder->setTheme(ThemeYellow::Instance);
	else folder->setTheme(ThemeBlack::Instance);

	inputRole->setTheme (theme);
	inputState->setTheme (theme);
	inputDirection->setTheme (theme);
	inputShootDir->setTheme (theme);
	inputPosition->setTheme (theme);
	inputBallPos->setTheme (theme);
	inputHead->setTheme (theme);
}

void RobotGUI::SetThemePen () {
	if(theme_handler) theme_handler = 0;
	else theme_handler = 1;
}

void RobotGUI::setGuiPosition(int x, int y)
{
	this->positionX = x;
	this->positionY = y;
	folder->setPosition(x,y);
}