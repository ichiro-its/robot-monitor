#pragma once

#include <string>
#include "ofMain.h"
#include "ofxNetwork.h"

class Listener {

public:

	ofxUDPManager udpConnection;
	bool newMessage;
	char message[1024];

	int id_kri;
	int id_rc;

	int port;
	bool showLog;
	FILE* file;

	Listener (int port, bool showLog);
	~Listener ();

	void Update ();
	void resetLog();
};