#include "Game.h"
#include "Themes.h"
#include "Field.h"

#include <bits/stdc++.h>

Game* Game::Instance;

Game::Game () {

	text = new ofTrueTypeFont();
	text->load("verdana.ttf", 40, true, true);

	text2 = new ofTrueTypeFont();
	text2->load("digital-7.ttf", 54, true, true);

	text3 = new ofTrueTypeFont();
	text3->load("digital-7.ttf", 54, true, true);

	lastMessage = "---";

	state = -1;
	scoreLeft = 0;
	scoreRight = 0;

	// label = new ofxDatGuiLabel ("GAME CONTROLLER LOG");
	// label->setPosition ((1366 - 960) / 2, 768 - (27 * 4));
	// label->setTheme (ThemeBlack::Instance);
	// label->setWidth (960 - (128 * 4));
	// label->update ();

	memset(penalSec,0,sizeof(penalSec));
	teamIndex = 0;
	enemyteamI = 1;
}

void Game::Update (Listener* listener) {

	if (!listener->newMessage) {
		return;
	}

	MessageData* referee = (MessageData*)listener->message;
	// printf("2nd state: %d %d %d %d\n", referee->secondary_state_info[0], referee->secondary_state_info[1], referee->secondary_state_info[2], referee->secondary_state_info[3]);
	// if(referee->version != 8) return;
	if(referee->teams[0].team_number == listener->id_kri || referee->teams[0].team_number == listener->id_rc) 
	{
		teamIndex = 0;
		enemyteamI = 1;
	}
	else 
	{
		teamIndex = 1;
		enemyteamI = 0;
	}

	for(int i = 0; i<=4; i++)
	{
		penal[i] = referee->teams[teamIndex].players[i].penalty;
		penalSec[i] = referee->teams[teamIndex].players[i].secs_till_unpenalised;
	}
	time_t timeRaw;
	tm* timeInfo;
	char timeBuffer[32];

	time (&timeRaw);
	timeInfo = localtime (&timeRaw);
	strftime (timeBuffer, 32, "%T", timeInfo);

	char message[256];

	int state, scoreLeft, scoreRight, minutes, secs, half, secstate, secstatei, Counter=0;

	state = referee->state;
	// score0 = referee->teams[0].score;
	// score1 = referee->teams[1].score;
	half = referee->first_half;
	secstate = referee->secondary_state;
	secstatei = referee->secondary_state_info[1];

	if(secStateInfo != secstatei)
	{
		if(secStateInfo == 2) 
		{
			secStateTime = referee->secs_remaining;
		}
		secStateInfo = secstatei;
	}

	if(this->state != state)
	{
		this->state = state;
		fprintf(listener->file, "%s   %s\n", timeBuffer, message);
	}

	if(referee->secondary_time)
	{
		char stat[1024];
		switch(this->state)
		{
			case INITIAL: sprintf(stat, "INITIAL"); break;
			case READY: sprintf(stat, "READY"); break;
			case PLAY: sprintf(stat, "PLAYING"); break;
			case FINISHED: sprintf(stat, "FINISHED"); break;
		}

		bool extratime = false;
		minutes = referee->secondary_time / 60;
		secs = referee->secondary_time%60;
		if(referee->secondary_time>600)
		{
			extratime = true;
			minutes = ((65536-referee->secondary_time)/60);
			secs = (65536-referee->secondary_time)%60;
		}
		sprintf (message, "%s (%s%.2d:%.2d)", stat, (extratime)? "-": "", minutes, secs);
		states = (std::string)message;
	}
	else
	{
		switch (state) {
			case INITIAL: sprintf (message, "INITIAL"); break;
			case READY: sprintf (message, "READY (00:%.2d)", referee->secondary_time); break;
			case SET: sprintf (message, "SET"); break;
			case PLAY: sprintf (message, "PLAYING"); break;
			case FINISHED: sprintf (message, "FINISHED"); break;
		}
		states = (std::string)message;
	}


	switch(secState)
	{
		case NORMAL: sprintf(message, "NORMAL"); break;
		case PENALTYSHOOT: sprintf(message, "PENALTYSHOOT"); break;
		case OVERTIME: sprintf(message, "OVERTIME"); break;
		case TIMEOUT: sprintf(message, "TIMEOUT"); break;
		case DIRECT_FREE_KICK: sprintf(message, "DIRECT FREE"); break;
		case INDIRECT_FREE_KICK: sprintf(message, "INDIRECT FREE"); break;
		case PENALTYKICK: sprintf(message, "PENALTY KICK"); break;
		case DROPBALL: sprintf(message, "DROPBALL"); break;
		case CORNER: sprintf(message, "CORNER"); break;
		case GOAL_KICK: sprintf(message, "GOAL KICK" ); break;
		case THROW_IN: sprintf(message, "THROW-IN"); break;

		default: sprintf(message, "UNKNOWN"); break;
	}


	if(this->secState != secstate)
	{
		secStatePrev = this->secState;
		this->secState = secstate;
		fprintf(listener->file, "%s   %s\n", timeBuffer, message);
	}
	if(secStateTime-referee->secs_remaining < 11 && referee->secs_remaining<=600)
	{
		switch(secStatePrev)
		{
			case NORMAL: sprintf(message, "NORMAL"); break;
			case PENALTYSHOOT: sprintf(message, "PENALTYSHOOT"); break;
			case OVERTIME: sprintf(message, "OVERTIME"); break;
			case TIMEOUT: sprintf(message, "TIMEOUT"); break;
			case DIRECT_FREE_KICK: sprintf(message, "DIRECT FREE"); break;
			case INDIRECT_FREE_KICK: sprintf(message, "INDIRECT FREE"); break;
			case PENALTYKICK: sprintf(message, "PENALTY KICK"); break;
			case DROPBALL: sprintf(message, "DROPBALL"); break;
			case CORNER: sprintf(message, "CORNER"); break;
			case GOAL_KICK: sprintf(message, "GOAL KICK" ); break;
			case THROW_IN: sprintf(message, "THROW-IN"); break;

			default: sprintf(message, "UNKNOWN"); break;
		}
		char temp[25];
		Counter=10-(secStateTime-referee->secs_remaining);
		sprintf(temp, " (%d)", Counter);
		strcat(message, temp);
	}

	secstates = (std::string)message;
	
	if(Field::Instance->IsFlipped())
	{
		scoreRight = referee->teams[teamIndex].score;
		scoreLeft = referee->teams[enemyteamI].score;
	}	
	else
	{
		scoreRight = referee->teams[enemyteamI].score;
		scoreLeft = referee->teams[teamIndex].score;
	}
	if (this->scoreLeft != scoreLeft || this->scoreRight != scoreRight) {
		this->scoreLeft = scoreLeft;
		this->scoreRight = scoreRight;

		sprintf (message, "%d - %d", scoreLeft, scoreRight);
		fprintf(listener->file, "%s   [ GOAL ] %s\n", timeBuffer, message);
		// scores = (std::string)message;
	}

	if(this->half!=half)
	{
		if(half) 
			this->half = 1;
		else this->half = 2;
		fprintf(listener->file, "%s   %s\n", timeBuffer, message);
		halfs = (std::string)message;
	}
	
	bool extratime = false;
	minute = referee->secs_remaining / 60;
	sec = referee->secs_remaining%60;
	if(referee->secs_remaining>600)
	{
		extratime = true;
		minute = ((65536-referee->secs_remaining)/60);
		sec = (65536-referee->secs_remaining)%60;
	}
	if(extratime)
		sprintf(message, "-%.2d:%.2d", minute, sec);
	else
		sprintf(message, "%.2d:%.2d", minute, sec);
	times = (std::string)message;
}

void Game::Draw (float ratioW, float ratioH) {

	// ofSetColor(8, 26, 34);
	// ofDrawRectangle(1366 / 2 - 115, (27 * 2) + 1, 230, 65); //score

	ofSetColor(0x000000); 
	ofDrawRectangle(250 * ratioW, (768 - (27 * 3)) * ratioH, 469 * ratioW, 62 * ratioH); //state
	ofDrawRectangle(720 * ratioW, (768 - (27 * 3)) * ratioH, 429 * ratioW, 62 * ratioH); //secstate

	ofSetColor(255);
	text3->drawString(std::to_string(half), (1366 / 2 - 20) * ratioW, 115 * ratioH); //half
	text3->drawString(times, (525 + (339/2) - (times.length()*20) + 20) * ratioW, 55 * ratioH); //time

	text2->drawString(std::to_string(scoreLeft), 445 * ratioW, 70 * ratioH); //score
	text2->drawString(std::to_string(scoreRight), 855 * ratioW, 70 * ratioH); //score

	text->drawString(states, (250 + (469/2) - (states.length()*40/4) - 60) * ratioW, (768 - (27 * 3) + 52)*ratioH); //state
	text->drawString(secstates, 740 * ratioW, (768 - (27 * 3) + 52)*ratioH); //secstate
}

void Game::Reset(Listener* listener)
{
	if(listener->newMessage) return;

	// scores = "0 - 0";
	states = "INITIAL";
	times = "10:00";
	secstates = "NORMAL";
}